local arr = std.range(1, 50);

local Job(index) = {
  script: "echo Hello",
  stage: 'stage' + index,
  needs: (if index > 1 then ['job' + (index - 1)] else []),
};

{ stages: ['stage' + x for x in arr] } +
{['job' + x]: Job(x) for x in arr}