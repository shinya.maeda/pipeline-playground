from setuptools import setup, find_packages

setup(
    name='myapi',
    version='0.1',
    description='A simple web API server using Flask',
    author='Your Name',
    author_email='your@email.com',
    url='https://github.com/yourusername/myapi',
    packages=find_packages(),
    install_requires=[
        'Flask',
        'Flask-RESTful',
    ],
)
